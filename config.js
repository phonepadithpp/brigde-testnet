module.exports = {
    app: {
        title: 'Chainsquare Plasma',
        port: 8000
    },
    chains: [
        {
            // EVM chain id: chainlist.org
            id: 5888,
            // Name of the chain
            name: 'Chainsqaure',
            // RPC URL of the chain
            rpc: 'https://v1seedtestnet.chainsquare.la/',
            // Enter deployed (bridge.sol) contract address
            contract: '0xa4BaaA5d391C397C38422D8fa21F8Dab1e114145',
            // Block explorer of chain
            explorer: 'https://testnet.chainsquare.la/tx/',
            // Gas price in WEI | 1 GWEI = 1000000000 WEI
            gasPrice: 6000000000,
            // Logo file name
            logo: 'chainsquare.png'
        },
        {
            id: 1999,
            name: 'LTC Chain',
            rpc: 'https://rpctestnet.laotel.com/',
            contract: '0xE1e6712D75E9510085AB7438D7a0C1110096230E',
            explorer: 'https://ltctestnet.laotel.com/tx/',
            gasPrice: 2000000000,
            logo: 'ltctestnet.png'
        },
        {
            id: 25925,
            name: 'Bitkub Chain',
            rpc: 'https://rpc-testnet.bitkubchain.io',
            contract: '0xa4BaaA5d391C397C38422D8fa21F8Dab1e114145',
            explorer: 'https://testnet.bkcscan.com/tx/',
            gasPrice: 6000000000,
            logo: 'bitkub.png'
        },
        {
            id: 80001,
            name: 'Polygon',
            rpc: 'https://rpc-mumbai.maticvigil.com/',
            contract: '0x998D965f331586fA35eFb2bd6701F8cC56b5D311',
            explorer: 'https://mumbai.polygonscan.com/tx/',
            gasPrice: 6000000000,
            logo: 'matic.png'
        }
    ],
    tokens: [
        {
            // Unique ID for the token
            id: 'chainsquare-multi',
            // Name of the token
            name: 'Chainsquare-Multi',
            // Symbol of the token
            symbol: 'CHMT',
            // Decimals of the token
            decimals: 18,
            // Minimum amount for swap
            min: 0.001,
            // Maximum amount for swap
            max: 1000000,
            address: {
                // chain id: token address in the chain
                5888: '0x998D965f331586fA35eFb2bd6701F8cC56b5D311',
                1999: '0x79c9522c9a62e9fC6a7BC68A8D51AD8C92C4B59C',
                25925: '0x998D965f331586fA35eFb2bd6701F8cC56b5D311',
                80001: '0xa4BaaA5d391C397C38422D8fa21F8Dab1e114145',
            }
        }
    ],
    // allow pairs for bridging. example: '1287-80001' to allow bridging from moonbase to polygon.
    pairs: ['5888-1999','1999-5888','5888-25925','25925-5888','1999-25925','25925-1999','5888-80001','1999-80001','25925-80001','80001-5888','80001-1999','80001-25925'],
    // fee multiplier. enter 0 for feeless transactions. (0.03 = 3% fee)
    feeMultiplier: 0,
    // private key of (bridge.sol) deployer address
    privateKey: ''
}